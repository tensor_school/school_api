# school_api

## Подготовка

Устаноите `pipenv`

Затем выполните:
```
pipenv install
```

## Запуск

```
pipenv run uvicorn app.main:app --reload
```
