"""
Работа с таблицей пользователей
"""

import sqlite3
from app.config import path_db
from app.model import User
from typing import List


def create_user(user: User) -> int:
    with sqlite3.connect(path_db) as conn:
        cursor = conn.execute(
            "INSERT INTO user (first_name, second_name, patronymic, city, phone, email, school, year, vk, facebook, telegram, whatsapp, photo) VALUES (:first_name, :second_name, :patronymic, :city, :phone, :email, :school, :year, :vk, :facebook, :telegram, :whatsapp, :photo)",
            {**dict(user)}
        )
        new_item_id = cursor.lastrowid
    conn.close()
    return new_item_id


def update_user(id: int, user: User) -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "UPDATE user SET first_name = :first_name, second_name = :second_name, patronymic = :patronymic, city = :city, phone = :phone, email = :email, school = :school, year = :year, vk = :vk, facebook = :facebook, telegram = :telegram, whatsapp = :whatsapp, photo = :photo WHERE id = :id",
            {**dict(user), "id": id}
        )
    conn.close()


def read_user(id: int) -> User:
    conn = sqlite3.connect(path_db)
    cursor = conn.execute(
        "SELECT id, first_name, second_name, patronymic, city, phone, email, school, year, vk, facebook, telegram, whatsapp, photo FROM user WHERE id = :id",
        {"id": id}
    )
    row = cursor.fetchone()
    conn.close()
    return row_to_user(row)


def delete_user(id: int) -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "DELETE FROM user WHERE id = :id",
            {"id": id}
        )
    conn.close()


def read_all_users() -> List[User]:
    conn = sqlite3.connect(path_db)
    cursor = conn.execute(
        "SELECT id, first_name, second_name, patronymic, city, phone, email, school, year, vk, facebook, telegram, whatsapp, photo FROM user",
        {"id": id}
    )
    rows = cursor.fetchall()
    conn.close()
    return list(map(row_to_user, rows))


def row_to_user(row) -> User:
    return User(
        id=row[0],
        first_name=row[1],
        second_name=row[2],
        patronymic=row[3],
        city=row[4],
        phone=row[5],
        email=row[6],
        school=row[7],
        year=row[8],
        vk=row[9],
        facebook=row[10],
        telegram=row[11],
        whatsapp=row[12],
        photo=row[13]
    )
