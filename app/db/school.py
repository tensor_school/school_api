"""
Работа с таблицей учебных заведений
"""

import sqlite3
from app.config import path_db
from app.model import School
from typing import List


def create_school(school: School) -> int:
    with sqlite3.connect(path_db) as conn:
        cursor = conn.execute(
            "INSERT INTO school (name, city) VALUES (:name, :city)",
            {**dict(school)}
        )
        new_item_id = cursor.lastrowid
    conn.close()
    return new_item_id


def update_school(id: int, school: School) -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "UPDATE school SET name = :name, city = :city WHERE id = :id",
            {**dict(school), "id": id}
        )
    conn.close()


def read_school(id: int) -> School:
    conn = sqlite3.connect(path_db)
    cursor = conn.execute(
        "SELECT id, name, city FROM school WHERE id = :id",
        {"id": id}
    )
    row = cursor.fetchone()
    conn.close()
    return row_to_school(row)


def delete_school(id: int) -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "DELETE FROM school WHERE id = :id",
            {"id": id}
        )
    conn.close()


def read_all_schools() -> List[School]:
    conn = sqlite3.connect(path_db)
    cursor = conn.execute(
        "SELECT id, name, city FROM school",
        {"id": id}
    )
    rows = cursor.fetchall()
    conn.close()
    return list(map(row_to_school, rows))


def row_to_school(row) -> School:
    return School(
        id=row[0],
        name=row[1],
        city=row[2]
    )
