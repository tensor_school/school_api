"""
Работа с таблицей городов
"""

import sqlite3
from app.config import path_db
from app.model import City
from typing import List


def create_city(city: City) -> int:
    with sqlite3.connect(path_db) as conn:
        cursor = conn.execute(
            "INSERT INTO city (name) VALUES (:name)",
            {**dict(city)}
        )
        new_item_id = cursor.lastrowid
    conn.close()
    return new_item_id


def update_city(id: int, city: City) -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "UPDATE city SET name = :name WHERE id = :id",
            {**dict(city), "id": id}
        )
    conn.close()


def read_city(id: int) -> City:
    conn = sqlite3.connect(path_db)
    cursor = conn.execute(
        "SELECT id, name FROM city WHERE id = :id",
        {"id": id}
    )
    row = cursor.fetchone()
    conn.close()
    return row_to_city(row)


def delete_city(id: int) -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "DELETE FROM city WHERE id = :id",
            {"id": id}
        )
    conn.close()


def read_all_cities() -> List[City]:
    conn = sqlite3.connect(path_db)
    cursor = conn.execute(
        "SELECT id, name FROM city",
        {"id": id}
    )
    rows = cursor.fetchall()
    conn.close()
    return list(map(row_to_city, rows))


def row_to_city(row) -> City:
    return City(
        id=row[0],
        name=row[1]
    )
