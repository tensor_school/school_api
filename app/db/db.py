"""
Модуль для работы с СУБД
"""

import sqlite3
from app.config import path_db


def create_tables() -> None:
    with sqlite3.connect(path_db) as conn:
        conn.execute(
            "CREATE TABLE IF NOT EXISTS city ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "name TEXT NOT NULL"
            ");"
        )
        conn.execute(
            "CREATE TABLE IF NOT EXISTS school ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "name TEXT NOT NULL, "
            "city INTEGER NOT NULL, "
            "FOREIGN KEY(city) REFERENCES city(id)"
            ");"
        )
        conn.execute(
            "CREATE TABLE IF NOT EXISTS user ("
            "id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "first_name TEXT NOT NULL, "
            "second_name TEXT NOT NULL, "
            "patronymic TEXT, "
            "city INTEGER NOT NULL, "
            "phone TEXT NOT NULL, "
            "email TEXT NOT NULL, "
            "school INTEGER NOT NULL, "
            "year INTEGER NOT NULL, "
            "vk TEXT, "
            "facebook TEXT, "
            "telegram TEXT, "
            "whatsapp TEXT, "
            "photo TEXT"
            ");"
        )
    conn.close()
