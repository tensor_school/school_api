from fastapi import FastAPI

from app.db import create_tables
from app.routes import city_router, school_router, user_router

create_tables()

app = FastAPI()

app.include_router(city_router)
app.include_router(school_router)
app.include_router(user_router)
