"""
Api роуты для User
"""

import app.db as db
from fastapi import APIRouter
from app.model import User
from typing import List

user_router = APIRouter(prefix="/user", tags=["User"])


@user_router.post("", response_model=int)
def create_user(user: User):
    return db.create_user(user)


@user_router.get("", response_model=List[User])
def read_all_users():
    return db.read_all_users()


@user_router.get("/{id}", response_model=User)
def read_user(id: int):
    return db.read_user(id)


@user_router.delete("/{id}")
def delete_user(id: int):
    return db.delete_user(id)


@user_router.put("/{id}")
def update_user(id: int, user: User):
    return db.update_user(id, user)
