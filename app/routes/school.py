"""
Api роуты для School
"""

import app.db as db
from fastapi import APIRouter
from app.model import School
from typing import List

school_router = APIRouter(prefix="/school", tags=["School"])


@school_router.post("", response_model=int)
def create_school(school: School):
    return db.create_school(school)


@school_router.get("", response_model=List[School])
def read_all_schools():
    return db.read_all_schools()


@school_router.get("/{id}", response_model=School)
def read_school(id: int):
    return db.read_school(id)


@school_router.delete("/{id}")
def delete_school(id: int):
    return db.delete_school(id)


@school_router.put("/{id}")
def update_school(id: int, school: School):
    return db.update_school(id, school)
