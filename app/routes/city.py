"""
Api роуты для City
"""

import app.db as db
from fastapi import APIRouter
from app.model import City
from typing import List

city_router = APIRouter(prefix="/city", tags=["City"])


@city_router.post("", response_model=int)
def create_city(city: City):
    return db.create_city(city)


@city_router.get("", response_model=List[City])
def read_all_cities():
    return db.read_all_cities()


@city_router.get("/{id}", response_model=City)
def read_city(id: int):
    return db.read_city(id)


@city_router.delete("/{id}")
def delete_city(id: int):
    return db.delete_city(id)


@city_router.put("/{id}")
def update_city(id: int, city: City):
    return db.update_city(id, city)
