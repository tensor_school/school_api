from pydantic import BaseModel
from typing import Optional


class User(BaseModel):
    id: Optional[int]
    first_name: str
    second_name: str
    patronymic: Optional[str]
    city: int
    phone: str
    email: str
    school: int
    year: int
    vk: Optional[str]
    facebook: Optional[str]
    telegram: Optional[str]
    whatsapp: Optional[str]
    photo: Optional[str]
