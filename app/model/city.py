from pydantic import BaseModel
from typing import Optional


class City(BaseModel):
    id: Optional[int]
    name: str
