from pydantic import BaseModel
from typing import Optional


class School(BaseModel):
    id: Optional[int]
    name: str
    city: int
